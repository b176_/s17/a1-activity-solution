// first part
let students = []

function addStudent(studentName)
{
    students.push(studentName)
    console.log(`${studentName} has been added to the list of students`)
}

function countStudents()
{
    console.log(`There are a total of ${students.length} students enrolled`)
}

function printStudents()
{
    students.forEach(function(student){
        console.log(student)
    })
}

function findStudent(studentName)
{
    let results = []

    students.filter(function(student){
        // find a match and push it to results
        if(student.toLowerCase().includes(studentName.toLowerCase()))
        {
            results.push(student)
        }
    })

    // checks number of results and prints text depending on number
    if(results.length == 1)
    {
        console.log(`${results[0]} is an enrollee`)
    }else if(results.length > 1)
    {
        console.log(`${results.join()} are enrollees`)
    }else{
        console.log(`${studentName} is not an enrollee`)
    }
}
// end of first part

// stretch goal
function addSection(section)
{ 
    students = students.map(student => student + ' - section ' + section)
}

function removeStudent(studentName)
{   
    let capitalizedStudentName = studentName.charAt(0).toUpperCase() + studentName.slice(1)

    students.splice(students.indexOf(capitalizedStudentName), 1)
    console.log(`${studentName} was removed from the student list`)
}
// end of stretch goal